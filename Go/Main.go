//Main.go
package main

import (
	"log"
	"net/http"
	"packers-movers/components"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/cors"
)

func main() {

	// Create a httprouter
	router := httprouter.New()

	// Add routes
	// POST /sign-in -> SignIn
	router.POST("/sign-in", components.SignIn)

	// POST /payment -> To calculate the esitmated cost of a journey
	router.POST("/payment", components.GetTripDetails)

	// GET /estimated-cost -> To push the esitmated cost of a journey
	router.GET("/estimated-cost", components.SendDetails)

	// POST /order -> To create an order
	router.POST("/order", components.CreateOrder)

	// GET /trip-details -> To push the trip details
	router.GET("/trip-details", components.SendTripDetails)

	// POST /new-driver -> To create a new driver
	router.POST("/new-driver", components.CreateDriver)

	// POST /new-customer -> To create a new customer
	router.POST("/new-customer", components.CreateUser)

	// POST /order-update -> To update an order
	router.POST("/order-update", components.UpdateOrder)

	handler := cors.Default().Handler(router)
	log.Fatal(http.ListenAndServe(":8080", handler))

}
