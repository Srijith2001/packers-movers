module packers-movers

go 1.16

require (
	github.com/confluentinc/confluent-kafka-go v1.8.2 // indirect
	github.com/gin-contrib/cors v1.3.1 // indirect
	github.com/gin-gonic/gin v1.7.7
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/google/uuid v1.3.0
	github.com/julienschmidt/httprouter v1.3.0 // indirect
	github.com/rs/cors v1.8.2 // indirect
	github.com/segmentio/ksuid v1.0.4 // indirect
	go.mongodb.org/mongo-driver v1.8.4 // indirect
	gorm.io/driver/postgres v1.3.1 // indirect
	gorm.io/gorm v1.23.3 // indirect
)
