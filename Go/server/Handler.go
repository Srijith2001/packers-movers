//Handler.go
package server

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type CustomerDetails struct {
	Name      string `json:"name"`
	Email     string `json:"email"`
	Password  string `json:"password"`
	Phone     string `json:"phone"`
	NoBooking string `json:"noBooking"`
	Id        string `json:"id"`
}

type VehicleDetails struct {
	Id         string `json:"id"`
	Brand      string `json:"brand"`
	Model      string `json:"model"`
	Vehicle_no string `json:"vehicle_no"`
	Driver_id  string `json:"driver_id"`
	Type       string `json:"type"`
	Location   struct {
		Door   string  `json:"door"`
		Street string  `json:"street"`
		City   string  `json:"city"`
		State  string  `json:"state"`
		Zip    string  `json:"zip"`
		Lat    float64 `json:"lat"`
		Long   float64 `json:"long"`
	}
	Distance        float64
	IsUnavailableOn []string
}

type VehicleTypes struct {
	Name         string `json:"name"`
	VehiclePrice string `json:"vehiclePrice"`
	PricePerKm   string `json:"pricePerKm"`
}

type DriverDetails struct {
	Name     string `json:"name"`
	Id       string `json:"id"`
	Contact  string `json:"contact"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type OrderDetails struct {
	Id   string `json:"id"`
	User CustomerDetails

	From struct {
		Door   string `json:"door"`
		Street string `json:"street"`
		City   string `json:"city"`
		State  string `json:"state"`
		Zip    string `json:"zip"`
		Lat    float64
		Long   float64
	}

	To struct {
		Door   string `json:"door"`
		Street string `json:"street"`
		City   string `json:"city"`
		State  string `json:"state"`
		Zip    string `json:"zip"`
		Lat    float64
		Long   float64
	}

	DateTime struct {
		Date string `json:"date"`
		Time string `json:"time"`
	}

	Vtype       string  `json:"vtype"`
	IsAvailable bool    `json:"isAvailable"`
	Assist      bool    `json:"assist"`
	Distance    float64 `json:"distance"`
	Ptype       string  `json:"ptype"`
	TotalCost   float64
	Bill        float64
}

func WriteJSONResponseWithData(data []byte, code int, w http.ResponseWriter) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Length, Content-Type, Accept, Authorization, access-control-allow-origin, access-control-allow-headers")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.WriteHeader(code)
	w.Write(data)
	fmt.Println("Response ", string(data))
}

func JsonifyMessage(msg interface{}, msgType string, httpCode int) ([]byte, int) {
	var data []byte
	var Obj struct {
		Status   string      `json:"status"`
		HTTPCode int         `json:"httpCode"`
		Message  interface{} `json:"message"`
	}
	Obj.Message = msg
	Obj.HTTPCode = httpCode
	switch msgType {
	case "ERR_MSG":
		Obj.Status = "FAILED"

	case "MSG":
		Obj.Status = "SUCCESS"
	}
	data, _ = json.Marshal(Obj)
	return data, httpCode
}

func WriteJSONStructWithData(v interface{}, w http.ResponseWriter) {
	d, err := json.Marshal(v)
	if err != nil {
		WriteJSONMessageWithData("Unable to marshal the response Data. Err: "+err.Error(), 500, w)
		return
	}
	WriteJSONResponseWithData(d, 200, w)
}

func WriteJSONMessageWithData(data string, code int, w http.ResponseWriter) {
	var Obj struct {
		Status   string `json:"status"`
		HTTPCode int    `json:"httpCode"`
		Message  string `json:"message"`
	}
	Obj.Message = data
	Obj.HTTPCode = code
	Obj.Status = "FAILED"
	d, _ := json.Marshal(Obj)
	fmt.Println("Error message ", d, code)
	WriteJSONResponseWithData(d, code, w)
}
