package server

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/go-redis/redis"
	guuid "github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
)

type newOrder struct {
	Id      string `json:"id"`
	User_id string `json:"user_id"`
	From    struct {
		Door   string  `json:"door"`
		Street string  `json:"street"`
		City   string  `json:"city"`
		State  string  `json:"state"`
		Zip    string  `json:"zip"`
		Lat    float64 `json:"lat"`
		Long   float64 `json:"long"`
	}
	To struct {
		Door   string  `json:"door"`
		Street string  `json:"street"`
		City   string  `json:"city"`
		State  string  `json:"state"`
		Zip    string  `json:"zip"`
		Lat    float64 `json:"lat"`
		Long   float64 `json:"long"`
	}
	DateTime struct {
		Date string `json:"date"`
		Time string `json:"time"`
	}
	Driver_id    string  `json:"driver_id"`
	Vehicle_id   string  `json:"vehicle_id"`
	Vehicle_type string  `json:"vehicle_type"`
	Bill         float64 `json:"bill"`
	Payment_type string  `json:"payment_type"`
	Trip_status  string  `json:"trip_status"`
}

func AddOrder(orderID string, vehicleDetails VehicleDetails) {

	// Get order details from redis
	var orderDetails OrderDetails

	val, err := redis.NewClient(&redis.Options{
		Addr: "localhost:6379",
	}).Get(orderID).Result()
	if err != nil {
		fmt.Println("Error retrieving order details from redis")
		return
	}
	err = json.Unmarshal([]byte(val), &orderDetails)
	if err != nil {
		fmt.Println("Error unmarshalling order details from redis")
		return
	}

	fmt.Println("Order Cost:", orderDetails.TotalCost)
	UpdateCustomer(orderDetails.User)

	client := conn_client()

	// Get a handle for your collection
	userCollection := client.Database("dataStore").Collection("customers")

	// Update Vehicles database
	UpdateVehicle(vehicleDetails.Id, orderDetails.DateTime.Date, orderDetails.Distance)

	// Filter the document with customer email
	var user CustomerDetails
	err = userCollection.FindOne(context.TODO(), bson.M{"email": orderDetails.User.Email}).Decode(&user)

	if err != nil {
		panic(err)
	}

	// Create a struct to push to kafka job queue
	var _order newOrder
	_order.Id = guuid.New().String()
	_order.User_id = user.Id
	_order.From.Door = orderDetails.From.Door
	_order.From.Street = orderDetails.From.Street
	_order.From.City = orderDetails.From.City
	_order.From.State = orderDetails.From.State
	_order.From.Zip = orderDetails.From.Zip
	_order.From.Lat = orderDetails.From.Lat
	_order.From.Long = orderDetails.From.Long
	_order.To.Door = orderDetails.To.Door
	_order.To.Street = orderDetails.To.Street
	_order.To.City = orderDetails.To.City
	_order.To.State = orderDetails.To.State
	_order.To.Zip = orderDetails.To.Zip
	_order.To.Lat = orderDetails.To.Lat
	_order.To.Long = orderDetails.To.Long
	_order.DateTime = orderDetails.DateTime
	_order.Driver_id = vehicleDetails.Driver_id
	_order.Vehicle_id = vehicleDetails.Id
	_order.Vehicle_type = vehicleDetails.Type
	_order.Bill = orderDetails.TotalCost
	_order.Payment_type = orderDetails.Ptype
	_order.Trip_status = "Booked"

	//Save job to kafka queue
	SaveJobToKafka(_order)
}

// Save job to kafka queue
func SaveJobToKafka(job newOrder) {
	fmt.Println("Start saving job to kafka queue")
	jsonString, err := json.Marshal(job)
	if err != nil {
		panic(err)
	}
	jobstring := string(jsonString)

	p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": "localhost:9092"})
	if err != nil {
		panic(err)
	}

	// Produce messages to topic (asynchronously)
	topic := "job_queue"
	for _, word := range []string{jobstring} {
		p.Produce(&kafka.Message{
			TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
			Value:          []byte(word),
		}, nil)
	}
	receiveFromKafka()
}

//kafka-mongo server
func receiveFromKafka() {
	fmt.Println("Start receiving from kafka queue")
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": "localhost:9092",
		"group.id":          "group1",
		"auto.offset.reset": "earliest",
	})

	if err != nil {
		panic(err)
	}

	c.SubscribeTopics([]string{"job_queue"}, nil)

	for {
		msg, err := c.ReadMessage(-1)

		if err == nil {
			fmt.Printf("Message on %s: %s\n", msg.TopicPartition, string(msg.Value))
			job := string(msg.Value)
			saveJobToMongo(job)
		} else {
			// The client will automatically try to recover from all errors.
			fmt.Printf("Consumer error: %v (%v)\n", err, msg)
			break
		}
	}

	c.Close()
}

func saveJobToMongo(jobString string) {
	fmt.Println("Start saving job to mongo")
	client := conn_client()

	// Get a handle for your collection
	jobCollection := client.Database("dataStore").Collection("orders")

	// Save data into Job struct
	var job newOrder
	b := []byte(jobString)
	err := json.Unmarshal(b, &job)
	if err != nil {
		panic(err)
	}

	// Insert data into mongo
	_, err = jobCollection.InsertOne(context.TODO(), job)
	if err != nil {
		panic(err)
	}

	fmt.Println("Job saved to mongo")
}
