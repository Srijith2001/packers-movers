package server

import (
	"log"
	"net/smtp"
)

func SendEmail(orderDetails OrderDetails, vehicleDetails VehicleDetails, driverDetails DriverDetails) {
	// Configuration
	from := "srijithbharadwajd@gmail.com"
	password := "srijith8603"
	to := []string{orderDetails.User.Email}
	smtpHost := "smtp.gmail.com"
	smtpPort := "587"

	// Prepare message
	message := []byte("You have successfully booked a trip with packers and movers.\n\n" + "Your vehicle details are:\n" + "\n" + "Vehicle Type: " + vehicleDetails.Type + "\n" + "Vehicle Model: " + vehicleDetails.Brand + " " + vehicleDetails.Model + "\n" + "Vehicle Number: " + vehicleDetails.Vehicle_no + "\n" + "Driver Name: " + driverDetails.Name + "\n" + "Driver Contact: " + driverDetails.Contact + "\n" + "\n" + "Thank you for using packers and movers.")

	// Create authentication
	auth := smtp.PlainAuth("", from, password, smtpHost)

	// Send actual message
	err := smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)
	if err != nil {
		log.Fatal(err)
	}

	// sendEmailToDriver(driverDetails, orderDetails)
}

func sendEmailToDriver(driverDetails DriverDetails, orderDetails OrderDetails) {
	// Configuration
	from := "srijithbharadwajd@gmail.com"
	password := "srijith8603"
	to := []string{driverDetails.Email}
	smtpHost := "smtp.gmail.com"
	smtpPort := "587"

	// Prepare message
	message := []byte("You have a new bookind:\n" + "From:\n" + orderDetails.From.Door + ",\n" + orderDetails.From.Street + ",\n" + orderDetails.From.City + ",\n" + orderDetails.From.Zip + "\n\n" + "To:\n" + orderDetails.To.Door + ",\n" + orderDetails.To.Street + ",\n" + orderDetails.To.City + ",\n" + orderDetails.To.Zip + "\n\n" + "Customer Name & Contact:" + orderDetails.User.Name + " - " + orderDetails.User.Phone + "\nJourney Date and Time: " + orderDetails.DateTime.Date + " " + orderDetails.DateTime.Time + "am\n\n" + "Thank you for using packers and movers.")

	// Create authentication
	auth := smtp.PlainAuth("", from, password, smtpHost)

	// Send actual message
	err := smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)
	if err != nil {
		log.Fatal(err)
	}
}

func SendEmailForRegistering(email string) {
	// Configuration
	from := "srijithbharadwajd@gmail.com"
	password := "srijith8603"
	to := []string{email}
	smtpHost := "smtp.gmail.com"
	smtpPort := "587"

	// Prepare message
	message := []byte("Thank you for registering with us!\n\n" + "We will provide updates through email.")

	// Create authentication
	auth := smtp.PlainAuth("", from, password, smtpHost)

	// Send actual message
	err := smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)
	if err != nil {
		log.Fatal(err)
	}

}
