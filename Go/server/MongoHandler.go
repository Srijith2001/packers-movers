package server

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"

	guuid "github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func conn_client() *mongo.Client {
	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb+srv://srijithd:srijith8603@cluster0.9gc5w.mongodb.net/test?authSource=admin&replicaSet=atlas-enhoa3-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")
	return client
}

func RetCosts(vtype string) (int, int) {

	// fmt.Println(vtype)
	client := conn_client()

	// Get a handle for your collection
	vehicleCollection := client.Database("dataStore").Collection("vehicle_types")

	// Filter the document with the desired vehicle type
	var vehicleType VehicleTypes
	err := vehicleCollection.FindOne(context.TODO(), bson.M{"name": vtype}).Decode(&vehicleType)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			// This error means your query did not match any documents.
			fmt.Println("No documents found")
		}
		panic(err)
	}

	vCost, _ := strconv.ParseInt(vehicleType.VehiclePrice, 10, 64)
	vCostpkm, _ := strconv.ParseInt(vehicleType.PricePerKm, 10, 64)
	return int(vCost), int(vCostpkm)
}

func RetVehicles(orderDetails OrderDetails) []VehicleDetails {
	client := conn_client()

	// Get a handle for vehicles collection
	vehicleCollection := client.Database("dataStore").Collection("vehicles")

	// Filter the document with the desired vehicle type and from location
	var vehicles []VehicleDetails
	filter := bson.M{"type": orderDetails.Vtype, "location.city": orderDetails.From.City}
	cursor, err := vehicleCollection.Find(context.TODO(), filter)
	if err != nil {
		panic(err)
	}

	if err = cursor.All(context.TODO(), &vehicles); err != nil {
		panic(err)
	}

	var allVehicles []VehicleDetails
	for _, vehicle := range vehicles {
		flag := true
		for _, date := range vehicle.IsUnavailableOn {
			fmt.Println(date, orderDetails.DateTime.Date)
			if date == orderDetails.DateTime.Date {
				flag = false
				break
			}
		}
		if flag {
			allVehicles = append(allVehicles, vehicle)
		}
	}
	return allVehicles
}

func RetDriverFromEmail(email string) DriverDetails {
	client := conn_client()

	// Get a handle for your collection
	driverCollection := client.Database("dataStore").Collection("drivers")

	// Filter the document with the desired vehicle type and from location
	var driver DriverDetails
	err := driverCollection.FindOne(context.TODO(), bson.M{"email": email}).Decode(&driver)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			// This error means your query did not match any documents.
			fmt.Println("No documents found")
		}
		panic(err)
	}

	return driver
}

func RetDriver(id string) DriverDetails {
	// Connect to client
	client := conn_client()

	// Get a handle for your collection
	driverCollection := client.Database("dataStore").Collection("drivers")

	// Filter the document with driver id
	var driver DriverDetails
	err := driverCollection.FindOne(context.TODO(), bson.M{"id": id}).Decode(&driver)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			// This error means your query did not match any documents.
			fmt.Println("No documents found")
		}
		panic(err)
	}
	fmt.Println(driver)
	return driver
}

type NewDriver struct {
	Driver struct {
		Name     string `json:"name"`
		Email    string `json:"email"`
		Contact  string `json:"contact"`
		Password string `json:"password"`
	}

	Vehicle struct {
		Vehicle_no string `json:"vehicle_no"`
		Brand      string `json:"brand"`
		Model      string `json:"model"`
		Type       string `json:"type"`
	}

	Location struct {
		Door   string  `json:"door"`
		Street string  `json:"street"`
		City   string  `json:"city"`
		State  string  `json:"state"`
		Zip    string  `json:"zip"`
		Lat    float64 `json:"lat"`
		Long   float64 `json:"long"`
	}
}

func AddDriver(newDriver NewDriver) bool {
	client := conn_client()

	// Get a handle for your collections
	userCollection := client.Database("dataStore").Collection("users")
	driverCollection := client.Database("dataStore").Collection("drivers")
	vehicleCollection := client.Database("dataStore").Collection("vehicles")

	// Create IDs for vehicle and driver
	dID := guuid.New().String()
	vID := guuid.New().String()

	// Add entry to user collection
	_, err := userCollection.InsertOne(context.TODO(), bson.D{
		{Key: "id", Value: dID},
		{Key: "category", Value: "driver"},
		{Key: "email", Value: newDriver.Driver.Email},
	})

	if err != nil {
		panic(err)
	}

	type Vehicle struct {
		Vehicle_no string `json:"vehicle_no"`
		Brand      string `json:"brand"`
		Model      string `json:"model"`
		Type       string `json:"type"`
		Id         string `json:"id"`
		Driver_id  string `json:"driver_id"`
		Location   struct {
			Door   string  `json:"door"`
			Street string  `json:"street"`
			City   string  `json:"city"`
			State  string  `json:"state"`
			Zip    string  `json:"zip"`
			Lat    float64 `json:"lat"`
			Long   float64 `json:"long"`
		}
	}

	type Driver struct {
		Id          string `bson:"id"`
		Name        string `bson:"name"`
		Email       string `bson:"email"`
		Password    string `bson:"password"`
		Contact     string `bson:"contact"`
		Vehicle_id  string `bson:"vehicle_id"`
		Rating      string `bson:"rating"`
		No_bookings string `bson:"no_bookings"`
	}

	// Add Vehicle to database
	var vehicle Vehicle
	vehicle.Vehicle_no = newDriver.Vehicle.Vehicle_no
	vehicle.Brand = newDriver.Vehicle.Brand
	vehicle.Model = newDriver.Vehicle.Model
	vehicle.Type = newDriver.Vehicle.Type
	vehicle.Id = vID
	vehicle.Driver_id = dID
	vehicle.Location.Door = newDriver.Location.Door
	vehicle.Location.Street = newDriver.Location.Street
	vehicle.Location.City = newDriver.Location.City
	vehicle.Location.State = newDriver.Location.State
	vehicle.Location.Zip = newDriver.Location.Zip
	vehicle.Location.Lat = newDriver.Location.Lat
	vehicle.Location.Long = newDriver.Location.Long

	_, err = vehicleCollection.InsertOne(context.TODO(), vehicle)
	if err != nil {
		panic(err)
	}

	// Add Driver to database
	var driver Driver
	driver.Id = dID
	driver.Name = newDriver.Driver.Name
	driver.Email = newDriver.Driver.Email
	driver.Password = newDriver.Driver.Password
	driver.Contact = newDriver.Driver.Contact
	driver.Vehicle_id = vID
	driver.Rating = "0"
	driver.No_bookings = "0"

	_, err = driverCollection.InsertOne(context.TODO(), driver)
	if err != nil {
		panic(err)
	}

	SendEmailForRegistering(driver.Email)
	fmt.Println("Added driver")
	return true
}

func AddCustomer(userDetails CustomerDetails) bool {
	client := conn_client()

	// Get a handle for your collections
	userCollection := client.Database("dataStore").Collection("users")
	customerCollection := client.Database("dataStore").Collection("customers")

	var user CustomerDetails
	err := customerCollection.FindOne(context.TODO(), bson.M{"email": userDetails.Email}).Decode(&user)
	if err == mongo.ErrNoDocuments {
		// This error means your query did not match any documents.
		userDetails.NoBooking = "0"
		customerCollection.InsertOne(context.TODO(), userDetails)

		// Add entry to user collection
		_, er := userCollection.InsertOne(context.TODO(), bson.D{
			{Key: "id", Value: userDetails.Id},
			{Key: "category", Value: "customer"},
			{Key: "email", Value: userDetails.Email},
		})

		if er != nil {
			panic(er)
		}
	} else {
		fmt.Println("User already exists")
		return false
	}
	return true
}

func UpdateCustomer(userDetails CustomerDetails) {
	client := conn_client()

	// Get a handle for your collection
	customerCollection := client.Database("dataStore").Collection("customers")

	var user CustomerDetails

	err := customerCollection.FindOne(context.TODO(), bson.M{"email": userDetails.Email}).Decode(&user)
	if err == mongo.ErrNoDocuments {
		// This error means your query did not match any documents.
		fmt.Println("No documents found")
		panic(err)
	} else {
		noBookings, _ := strconv.ParseInt(user.NoBooking, 10, 64)
		noBookings = int64(noBookings) + 1
		userDetails.NoBooking = strconv.FormatInt(noBookings, 10)
		customerCollection.UpdateOne(context.TODO(), bson.M{"email": userDetails.Email}, bson.M{"$set": bson.M{"nobooking": userDetails.NoBooking}})
	}
}

func GetCustomer(email string) CustomerDetails {
	// Connect to client
	client := conn_client()

	// Get a handle for your collection
	customerCollection := client.Database("dataStore").Collection("customers")

	// Filter the document with customer id
	var customer CustomerDetails
	err := customerCollection.FindOne(context.TODO(), bson.M{"email": email}).Decode(&customer)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			// This error means your query did not match any documents.
			fmt.Println("No documents found")
		}
		panic(err)
	}

	return customer
}

func GetTypeOfUser(email string) (string, bool) {
	client := conn_client()

	// Get a handle for your collection
	userCollection := client.Database("dataStore").Collection("users")

	// Filter the document with driver id
	var types struct {
		Category string `json:"category"`
		Id       string `json:"id"`
		Email    string `json:"email"`
	}
	err := userCollection.FindOne(context.TODO(), bson.M{"email": email}).Decode(&types)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			// This error means your query did not match any documents.
			fmt.Println("No documents found")
			return "", false
		}
	}

	return types.Category, true

}

func GetOrdersAsCustomer(id string) []OrderDetails {
	client := conn_client()

	// Get a handle for your collection
	orderCollection := client.Database("dataStore").Collection("orders")

	// Filter the document with customer id
	var orders []OrderDetails

	cur, err := orderCollection.Find(context.TODO(), bson.M{"user_id": id})
	if err != nil {
		panic(err)
	}

	for cur.Next(context.TODO()) {
		var order OrderDetails
		err := cur.Decode(&order)
		if err != nil {
			panic(err)
		}
		orders = append(orders, order)
	}

	if err := cur.Err(); err != nil {
		panic(err)
	}

	cur.Close(context.TODO())

	return orders
}

func GetOrdersAsDriver(id string) []OrderDetails {
	client := conn_client()

	// Get a handle for your collection
	orderCollection := client.Database("dataStore").Collection("orders")

	// Filter the document with driver id
	var orders []OrderDetails

	cur, err := orderCollection.Find(context.TODO(), bson.M{"driver_id": id})
	if err != nil {
		panic(err)
	}

	for cur.Next(context.TODO()) {
		var order OrderDetails
		err := cur.Decode(&order)
		if err != nil {
			panic(err)
		}
		orders = append(orders, order)
	}

	if err := cur.Err(); err != nil {
		panic(err)
	}

	cur.Close(context.TODO())

	return orders
}

func UpdateVehicle(id string, date string, distance float64) {
	client := conn_client()

	// Get a handle for your collection
	vehicleCollection := client.Database("dataStore").Collection("vehicles")

	// Filter the document with driver id
	var vehicle VehicleDetails
	err := vehicleCollection.FindOne(context.TODO(), bson.M{"id": id}).Decode(&vehicle)
	if err != nil {
		panic(err)
	}
	if distance > 100 {
		splited := strings.Split(date, "/")
		d, _ := strconv.Atoi(splited[0])
		m, _ := strconv.Atoi(splited[1])
		y, _ := strconv.Atoi(splited[2])
		fmt.Println("Dates truck: ", d, m, y)
		d = d + 1
		if d > 30 {
			d = 1
			m = m + 1
			if m > 12 {
				m = 1
				y = y + 1
			}
		}
		var dateNext string
		if m < 10 {
			dateNext = strconv.Itoa(d) + "/0" + strconv.Itoa(m) + "/" + strconv.Itoa(y)
		} else {
			dateNext = strconv.Itoa(d) + "/" + strconv.Itoa(m) + "/" + strconv.Itoa(y)
		}
		vehicle.IsUnavailableOn = append(vehicle.IsUnavailableOn, date)
		vehicle.IsUnavailableOn = append(vehicle.IsUnavailableOn, dateNext)
	} else {
		vehicle.IsUnavailableOn = append(vehicle.IsUnavailableOn, date) //Add a single day to unavailable list
	}

	fmt.Println("Vehicle unavailable on: ", vehicle.IsUnavailableOn)

	vehicleCollection.UpdateOne(context.TODO(), bson.M{"id": id}, bson.M{"$set": bson.M{"isUnavailableOn": vehicle.IsUnavailableOn}})
}
func SetOrderStatus(id string, status string) {
	fmt.Println(id, status)
	client := conn_client()

	// Get a handle for your collection
	orderCollection := client.Database("dataStore").Collection("orders")

	// Filter the document with driver id
	orderCollection.UpdateOne(context.TODO(), bson.M{"id": id}, bson.M{"$set": bson.M{"trip_status": status}})
}
