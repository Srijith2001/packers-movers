package components

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
)

func computeCoordinates(Address string) <-chan Coordinates {

	c := make(chan Coordinates)
	go func() {
		baseURL, _ := url.Parse("http://www.mapquestapi.com/geocoding/v1/address?")

		params := url.Values{}
		// Access Key
		params.Add("key", "p8gcz3suqTxqOKgm9zBz2xFuMa0y3auu")
		// Query
		params.Add("location", Address)

		baseURL.RawQuery = params.Encode()

		req, _ := http.NewRequest("GET", baseURL.String(), nil)

		res, _ := http.DefaultClient.Do(req)

		defer res.Body.Close()

		body, _ := ioutil.ReadAll(res.Body)

		m := make(map[string]interface{})
		err := json.Unmarshal(body, &m)
		if err != nil {
			log.Fatal(err)
		}

		latitude, err := strconv.ParseFloat(fmt.Sprint(m["results"].([]interface{})[0].(map[string]interface{})["locations"].([]interface{})[0].(map[string]interface{})["latLng"].(map[string]interface{})["lat"]), 64)
		if err != nil {
			log.Fatal(err)
		}
		longitude, err := strconv.ParseFloat(fmt.Sprint(m["results"].([]interface{})[0].(map[string]interface{})["locations"].([]interface{})[0].(map[string]interface{})["latLng"].(map[string]interface{})["lng"]), 64)
		if err != nil {
			log.Fatal(err)
		}
		c <- Coordinates{latitude, longitude}
	}()

	return c

}
