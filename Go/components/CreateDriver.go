package components

import (
	"encoding/json"
	"net/http"
	"packers-movers/server"

	"github.com/julienschmidt/httprouter"
)

func CreateDriver(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	defer r.Body.Close()

	// Decode the request body into a struct
	var newDriver server.NewDriver
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&newDriver)
	if err != nil {
		server.WriteJSONMessageWithData("Unable to unmarshal the request", 400, w)
		return
	}

	// Compute Coordinates of the new driver's addresss
	address := newDriver.Location.Door + "," + newDriver.Location.Street + "," + newDriver.Location.City + "," + newDriver.Location.State + "," + newDriver.Location.Zip
	latlng := <-computeCoordinates(address)
	newDriver.Location.Lat = latlng.Lat
	newDriver.Location.Long = latlng.Long

	// Add details to the database
	if server.AddDriver(newDriver) {
		// Write response to the client
		server.WriteJSONStructWithData(&newDriver, w)
	} else {
		var resp PostResponse
		resp.Message = "Driver already exists"
		server.WriteJSONStructWithData(&resp, w)
	}
}
