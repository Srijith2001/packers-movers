package components

import (
	"encoding/json"
	"fmt"
	"net/http"
	"packers-movers/server"
	"sort"

	"github.com/go-redis/redis"
	"github.com/julienschmidt/httprouter"
)

type Request struct {
	Ptype string `json:"ptype"`
}

// DistanceSorter sorts vehicles by distance.
type DistanceSorter []server.VehicleDetails

func (a DistanceSorter) Len() int           { return len(a) }
func (a DistanceSorter) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a DistanceSorter) Less(i, j int) bool { return a[i].Distance < a[j].Distance }

var NearestVehicle server.VehicleDetails
var AllVehicles []server.VehicleDetails

func estimateCost(orderDetails server.OrderDetails) chan float64 {
	cost := make(chan float64)
	go func() {
		fmt.Println("Order Distance: ", orderDetails.Distance)
		vcost, costpkm := server.RetCosts(orderDetails.Vtype)
		totalCost := float64(vcost) + float64(costpkm)*orderDetails.Distance
		cost <- totalCost
	}()
	return cost
}

func CreateOrder(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	defer r.Body.Close()

	// Decode the request body into a struct
	var req Request
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&req)
	if err != nil {
		server.WriteJSONMessageWithData("Unable to unmarshal the request", 400, w)
		return
	}

	// Write response to the client
	response := PostResponse{}
	server.WriteJSONStructWithData(&response, w)

	order.Ptype = req.Ptype

	// Feed the distance from the order origin location
	for i, vehicle := range AllVehicles {
		AllVehicles[i].Distance = <-distance(vehicle.Location.Lat, vehicle.Location.Long, order.From.Lat, order.From.Long, "K")
	}

	// Sort the array of vehicles by distance
	sort.Sort(DistanceSorter(AllVehicles))

	// Find the nearest vehicle
	NearestVehicle = AllVehicles[0]

	// Add Order to database through kafka
	server.AddOrder(order.Id, NearestVehicle)
}

func SendDetails(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	defer r.Body.Close()
	fmt.Println("Ping Success")

	// Get previously stored order details from redis
	var order server.OrderDetails
	val, err := redis.NewClient(&redis.Options{
		Addr: "localhost:6379",
	}).Get(GetOrderId()).Result()
	if err != nil {
		fmt.Println("Error retrieving order details from redis")
		return
	}
	err = json.Unmarshal([]byte(val), &order)
	if err != nil {
		fmt.Println("Error unmarshalling order details from redis")
		return
	}
	order.TotalCost = <-estimateCost(order)
	AllVehicles = server.RetVehicles(order)
	if len(AllVehicles) == 0 {
		order.IsAvailable = false
	} else {
		order.IsAvailable = true
	}

	if order.Assist {
		order.TotalCost += 1000
	}

	// Update the order details to redis
	json, err := json.Marshal(order)
	if err != nil {
		fmt.Println("Error marshalling order\n", err)
	}

	err = redis.NewClient(&redis.Options{
		Addr: "localhost:6379",
	}).Set(order.Id, json, 0).Err()
	if err != nil {
		fmt.Println("Error saving order to redis.\n", err)
	}

	d, code := server.JsonifyMessage(order, "MSG", http.StatusOK)
	server.WriteJSONResponseWithData(d, code, w)
}

func SendTripDetails(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	defer r.Body.Close()

	driverDetails := server.RetDriver(NearestVehicle.Driver_id)
	var sendDetails struct {
		OrderD  server.OrderDetails   `json:"orderDetails"`
		Vehicle server.VehicleDetails `json:"nearestVehicle"`
		Driver  server.DriverDetails  `json:"driverDetails"`
	}
	sendDetails.OrderD = order
	sendDetails.Vehicle = NearestVehicle
	sendDetails.Driver = driverDetails
	d, code := server.JsonifyMessage(sendDetails, "MSG", http.StatusOK)
	server.WriteJSONResponseWithData(d, code, w)

	// Send Email to customer
	server.SendEmail(order, NearestVehicle, driverDetails)
}

func UpdateOrder(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	defer r.Body.Close()

	// Decode the request body into a struct
	var req struct {
		OrderId string `json:"orderId"`
		Status  string `json:"status"`
	}

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&req)
	if err != nil {
		server.WriteJSONMessageWithData("Unable to unmarshal the request", 400, w)
		return
	}

	fmt.Println("Order Id: ", req.OrderId)
	fmt.Println("Status: ", req.Status)
	// Update the order status in the database
	server.SetOrderStatus(req.OrderId, req.Status)

	// Write response to the client
	response := PostResponse{
		Message: "Order status updated",
	}
	server.WriteJSONStructWithData(&response, w)
}
