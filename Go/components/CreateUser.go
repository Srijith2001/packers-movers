package components

import (
	"encoding/json"
	"fmt"
	"net/http"
	"packers-movers/server"

	"github.com/julienschmidt/httprouter"
)

func CreateUser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	defer r.Body.Close()

	// Decode the request body into a struct
	var newUser server.CustomerDetails
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&newUser)
	if err != nil {
		server.WriteJSONMessageWithData("Unable to unmarshal the request", 400, w)
		return
	}

	// Add details to the database
	success := server.AddCustomer(newUser)
	fmt.Println("Success: ", success)
	if success {
		// Write response to the client
		server.WriteJSONStructWithData(&newUser, w)
	} else {
		var resp PostResponse
		resp.Message = "User already exists"
		server.WriteJSONStructWithData(&resp, w)
	}
}
