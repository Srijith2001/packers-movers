package components

import (
	"encoding/json"
	"fmt"
	"net/http"
	"packers-movers/server"

	"github.com/julienschmidt/httprouter"
)

type Details struct {
	Customer   server.CustomerDetails
	Driver     server.DriverDetails
	Orders     []server.OrderDetails
	TypeOfUser string
}

var PushDetails Details

type UserRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func SignIn(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	defer r.Body.Close()

	// Decode the request body into a struct
	var userreq UserRequest
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&userreq)
	if err != nil {
		server.WriteJSONMessageWithData("Unable to unmarshal the request", 400, w)
		return
	}

	// Fetch the user from the database
	typeOfUser, exists := server.GetTypeOfUser(userreq.Email)
	PushDetails.TypeOfUser = typeOfUser
	fmt.Println("Users exists: ", exists)
	if exists {
		if typeOfUser == "customer" {
			customer := server.GetCustomer(userreq.Email)
			if customer.Password == userreq.Password {
				PushDetails.Customer = customer
				PushDetails.Orders = server.GetOrdersAsCustomer(PushDetails.Customer.Id)
			} else {
				server.WriteJSONMessageWithData("Invalid Password", 400, w)
			}
		} else if typeOfUser == "driver" {
			driver := server.RetDriverFromEmail(userreq.Email)
			if driver.Password == userreq.Password {
				PushDetails.Driver = driver
				fmt.Println("Driver ID: ", PushDetails.Driver.Id)
				PushDetails.Orders = server.GetOrdersAsDriver(PushDetails.Driver.Id)
			} else {
				server.WriteJSONMessageWithData("Invalid Password", 400, w)
			}
		}
		// Write response to the client
		server.WriteJSONStructWithData(&PushDetails, w)
	} else {
		var resp PostResponse
		resp.Message = "failed"
		server.WriteJSONStructWithData(&resp, w)
	}
}
