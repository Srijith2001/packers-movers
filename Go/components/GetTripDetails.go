package components

import (
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"packers-movers/server"

	"github.com/go-redis/redis"
	guuid "github.com/google/uuid"
	"github.com/julienschmidt/httprouter"
)

type Coordinates struct {
	Lat  float64
	Long float64
}

type PostResponse struct {
	Message string `json:"message"`
}

var order server.OrderDetails

func GetTripDetails(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	defer r.Body.Close()

	// Decode the request body into a struct
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&order)
	if err != nil {
		server.WriteJSONMessageWithData("Unable to unmarshal the request", 400, w)
		return
	}

	// Convert from address into lat/long
	FromAddress := order.From.Door + "," + order.From.Street + "," + order.From.City + "," + order.From.State + "," + order.From.Zip
	latlng := <-computeCoordinates(FromAddress)
	order.From.Lat = latlng.Lat
	order.From.Long = latlng.Long

	// Convert to address into lat/long
	ToAddress := order.To.Door + "," + order.To.Street + "," + order.To.City + "," + order.To.State + "," + order.To.Zip
	latlng = <-computeCoordinates(ToAddress)
	order.To.Lat = latlng.Lat
	order.To.Long = latlng.Long

	d := <-distance(order.From.Lat, order.From.Long, order.To.Lat, order.To.Long, "K")
	order.Distance = math.Ceil(d)

	// Write response to the client
	server.WriteJSONStructWithData(&order, w)
	fmt.Println("Posting user details successful")

	fmt.Println("Saving order details to redis")

	// Assign Id to our new order
	order.Id = guuid.New().String()

	// Create a redis client
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	// Save the order details to redis
	json, err := json.Marshal(order)
	if err != nil {
		fmt.Println("Error marshalling order\n", err)
	}

	err = client.Set(order.Id, json, 0).Err()
	if err != nil {
		fmt.Println("Error saving order to redis.\n", err)
	}

}

func GetOrderId() string {
	return order.Id
}
