import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import Trucks from "../Trucks";

const MockTrucks = (props) => {
	return (
		<BrowserRouter>
			<Trucks {...props} />
		</BrowserRouter>
	);
};
describe("Trucks", () => {
	it("Check if everything renders properly", () => {
		render(<MockTrucks setCard={() => {}} />);
		expect(screen.getByText("3 Wheeler")).toBeInTheDocument();
	});
});
