import React from "react";

import threeWheeler from "../../images/vehicles/3_wheeler.png";
import miniTruck from "../../images/vehicles/mini_truck.jpg";
import pickup from "../../images/vehicles/pickup.jpg";
import tractor from "../../images/vehicles/tractor.jpg";
import trailer from "../../images/vehicles/trailer.jpg";
import truck from "../../images/vehicles/truck.png";
import Card from "../Card/Card";
import { Container, Row, Col } from "reactstrap";
import { Link } from "react-router-dom";

export default function Trucks(props) {
	const vehicles = [
		{
			name: "3 Wheeler",
			image: threeWheeler,
			desc: "Can carry upto 200 kgs.",
			vehiclePrice: "₹ 500",
			pricePKm: "₹ 20",
			id: 1,
		},
		{
			name: "Mini Truck",
			image: miniTruck,
			desc: "Can carry upto 500 kgs.",
			vehiclePrice: "₹ 1000",
			pricePKm: "₹ 30",
			id: 2,
		},
		{
			name: "Pickup",
			image: pickup,
			desc: "Can carry upto 1000 kgs.",
			vehiclePrice: "₹ 1500",
			pricePKm: "₹ 35",
			id: 3,
		},
		{
			name: "Truck",
			image: truck,
			desc: "Can carry upto 2000 kgs.",
			vehiclePrice: "₹ 2000",
			pricePKm: "₹ 40",
			id: 4,
		},
		{
			name: "Tractor",
			image: tractor,
			desc: "Can carry upto 5000 kgs.",
			vehiclePrice: "₹ 2500",
			pricePKm: "₹ 45",
			id: 5,
		},
		{
			name: "Trailer",
			image: trailer,
			desc: "Can carry upto 10000 kgs.",
			vehiclePrice: "₹ 3000",
			pricePKm: "₹ 50",
			id: 6,
		},
	];

	function handleClick(e, name) {
		props.setCard(name);
	}

	const cards = vehicles.map((item) => {
		return (
			<Col key={item.id}>
				<Link to="user-details" className="text-link">
					<Card
						image={item.image}
						name={item.name}
						desc={item.desc}
						vehiclePrice={item.vehiclePrice}
						pricePKm={item.pricePKm}
						onClick={(event) => handleClick(event, item.name)}
					/>
				</Link>
			</Col>
		);
	});

	return (
		<section className="right-pane">
			<h3>Trucks Available:</h3>
			<p>Pick the type of vehicle you would require:</p>
			<section className="cards-list">
				<Container>
					<Row xs={3}>{cards}</Row>
				</Container>
			</section>
		</section>
	);
}
