import React from "react";
import { nanoid } from "nanoid";
import { Link } from "react-router-dom";

import logo from "../../images/logo_icon.png";

export default function Navbar(props) {
	const menuEle = props.menuList.map((item) => {
		return (
			<Link
				to={item.href}
				key={nanoid()}
				className={item.active ? "nav-item active" : "nav-item"}
				onClick={() => props.setMenuTab(item.val)}
			>
				{item.val}
			</Link>
		);
	});

	if (props.isSignedIn && props.typeOfUser === "customer") {
		menuEle.push(
			<Link
				to={"/profile"}
				key={nanoid()}
				className={
					props.menuTab === "Profile" ? "nav-item active" : "nav-item"
				}
				onClick={() => props.setMenuTab("Profile")}
			>
				Profile
			</Link>
		);
	} else if (props.isSignedIn && props.typeOfUser === "driver") {
		menuEle.push(
			<Link
				to={"/yourorders"}
				key={nanoid()}
				className={
					props.menuTab === "Dashboard"
						? "nav-item active"
						: "nav-item"
				}
				onClick={() => props.setMenuTab("Dashboard")}
			>
				Dashboard
			</Link>
		);
	}
	return (
		<>
			<div
				className="navbar navbar-expand-lg navbar-light bg-light"
				role="navigation"
			>
				<a className="navbar-brand" href="/">
					<img src={logo} alt="logo" className="nav-logo" />
					Packers & Movers
				</a>
				<button
					className="navbar-toggler custom-toggler"
					type="button"
					data-toggle="collapse"
					data-target="#navEle"
					aria-controls="navEle"
					aria-expanded="false"
					aria-label="Toggle navigation"
				>
					<span className="line"></span>
					<span className="line"></span>
					<span className="line" style={{ marginBottom: "0" }}></span>
				</button>
				<div
					className="nav-links collapse navbar-collapse justify-content-end"
					id="navEle"
				>
					{menuEle}
				</div>
			</div>
			<hr></hr>
		</>
	);
}
