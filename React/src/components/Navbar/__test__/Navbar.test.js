import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import Navbar from "../Navbar";

const props = {
	menuList: [
		{
			val: "Home",
			href: "/",
			active: true,
		},
		{
			val: "About",
			href: "/about",
			active: false,
		},
		{
			val: "",
			href: "/",
			active: false,
		},
	],
	typeOfUser: "customer",
	isSignedIn: true,
	menuTab: "Home",
	setMenuTab: jest.fn(),
};

const MockNavbar = (props) => {
	return (
		<BrowserRouter>
			<Navbar {...props} />
		</BrowserRouter>
	);
};
describe("Navbar", () => {
	it("renders navbar with the given props", () => {
		render(<MockNavbar {...props} />);
		const homeTab = screen.getByText("Home");
		expect(homeTab).toBeInTheDocument();
	});
});
