import { fireEvent, render, screen } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import OrderEle from "../OrderEle";

const props = [
	{
		order: {
			id: "test",
			Bill: 2000,
			From: {
				door: "",
				street: "",
				city: "",
			},
			To: {
				door: "",
				street: "",
				city: "",
			},
			DateTime: {
				date: "",
			},
		},
		TypeOfUser: "customer",
	},
	{
		order: {
			id: "test2",
			Bill: 5000,
			From: {
				door: "",
				street: "",
				city: "",
			},
			To: {
				door: "",
				street: "",
				city: "",
			},
			DateTime: {
				date: "",
			},
		},
		TypeOfUser: "driver",
	},
];

describe("OrderEle", () => {
	it("Should render order element details", () => {
		props.forEach((prop) => {
			render(<OrderEle {...prop} />);
			if (prop.TypeOfUser === "driver") {
				const tripStartedBtn = screen.getByText(/Trip Started/i);
				fireEvent.click(tripStartedBtn);
				const tripEndedBtn = screen.getByText(/Trip Ended/i);
				fireEvent.click(tripEndedBtn);
				// eslint-disable-next-line jest/no-conditional-expect
				const tripEndedText = screen.getByRole("heading", {
					name: /Trip Ended/i,
				});
				// eslint-disable-next-line jest/no-conditional-expect
				expect(tripEndedText).toBeInTheDocument();
			} else {
				const orderBill = screen.getByText(/2000/i);
				// eslint-disable-next-line jest/no-conditional-expect
				expect(orderBill).toBeInTheDocument();
			}
		});
	});
});
