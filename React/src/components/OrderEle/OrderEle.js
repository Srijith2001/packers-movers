import React from "react"
import { Button } from "@mui/material"
import {nanoid} from "nanoid"

export default function OrderEle({order, TypeOfUser}) {

    const [isTripStarted, setIsTripStarted] = React.useState(false)
    const [isTripEnded, setIsTripEnded] = React.useState(false)

    function handleTripStart(e,id) {
        var ostatus= isTripStarted ? "" : "Started"
        const requestOptions = {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                orderId: id,
                status: ostatus
            })
        }
        fetch("http://localhost:8080/order-update", requestOptions).then(response => response.json())
        .then(data => {
            console.log(data)
        })

        setIsTripStarted(prevState => !prevState)
    }

    function handleTripEnd (e,id) {

        const requestOptions = {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                orderId: id,
                status: isTripEnded ? "" : "Completed"
            })
        }

        fetch("http://localhost:8080/order-update", requestOptions).then(response => response.json())
        .then(data => {
            console.log(data)
        })

        setIsTripEnded(prevState => !prevState)
    }

    return (
        <div key={nanoid} className="order">
            <div className="order-details">
                <p>Order ID: {order.id} <br/>
                Total Cost:  Rs.{order.Bill}</p>
            </div>
            <div>
                <div className="user-orders">
                    <div className="user-order-from">
                        <h5 className="bold">From</h5>
                        <p>{order.From.door}, <br/>
                        {order.From.street},<br/>
                        {order.From.city}</p>
                    </div>

                    <div className="user-order-from">
                        <h5 className="bold">To</h5>
                        <p>{order.To.door}, <br/>
                        {order.To.street},<br/>
                        {order.To.city}</p>
                    </div>

                    <div className="user-order-from">
                        <h5 className="bold">Date of Journey</h5>
                        <p>{order.DateTime.date}</p>
                    </div>
                    {TypeOfUser === "driver" ?
                    <>
                    <div className="user-order-from">
                        <Button
                            variant="contained"
                            onClick={(e) => handleTripStart(e, order.id)}
                            disabled={isTripStarted}
                        >Trip Started</Button>
                        <Button
                            variant="contained"
                            onClick={(e) => handleTripEnd(e, order.id)}
                            style={{margin: "10px"}}
                            disabled={!isTripStarted || isTripEnded}
                        >Trip Ended</Button>
                    </div>
                    </>
                    :
                    null}
                    {isTripEnded ?
                    <div className="user-order-from">
                        <h5>Trip Ended</h5>
                    </div>
                    :
                    null}
                </div>
            </div>
        </div>
    )
}