import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHouse } from "@fortawesome/free-solid-svg-icons";
import { faHomeLg } from "@fortawesome/free-solid-svg-icons";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import { DateTimePicker } from "@mui/lab";
import { Button, TextField } from "@mui/material";
import { useNavigate } from "react-router-dom";

export default function Sidebar(props) {
	const navigate = useNavigate();
	function handleSubmit(e) {
		e.preventDefault();
		props.setIsSubmit(true);
		navigate("trucks");
	}
	return (
		<section
			data-testid="sidebar-section"
			className={props.isSubmit ? "left-pane pane" : "center"}
		>
			<div className="form-from">
				<FontAwesomeIcon icon={faHouse} />
				<label htmlFor="shift-from">FROM</label>
				<br></br>
				<input
					id="shift-from"
					type="text"
					list="input-from-list"
					value={props.from}
					onChange={(event) => props.setFrom(event.target.value)}
					placeholder="Enter pickup city"
				/>
			</div>
			<br></br>
			<div className="form-to">
				<FontAwesomeIcon icon={faHomeLg} />
				<label htmlFor="shift-to">TO</label>
				<br></br>
				<input
					id="shift-to"
					type="text"
					list="input-from-list"
					value={props.to}
					onChange={(event) => props.setTo(event.target.value)}
					placeholder="Enter destination city"
				/>
			</div>
			<div className="form-date">
				<LocalizationProvider dateAdapter={AdapterDateFns}>
					<DateTimePicker
						label="Date and Time of pickup"
						type="datetime-local"
						value={props.date}
						onChange={props.setDate}
						renderInput={(params) => <TextField {...params} />}
						inputFormat="dd/MM/yyyy h:mm a"
					/>
				</LocalizationProvider>
			</div>
			<div className="form-submit">
				<Button variant="contained" onClick={handleSubmit}>
					SUBMIT
				</Button>
			</div>
		</section>
	);
}
