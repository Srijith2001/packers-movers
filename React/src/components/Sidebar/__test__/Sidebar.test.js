import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import Sidebar from "../Sidebar";

const MockSidebar = (props) => {
	return (
		<BrowserRouter>
			<Sidebar {...props} />
		</BrowserRouter>
	);
};
describe("Sidebar", () => {
	it("Should render the sidebar", () => {
		render(
			<MockSidebar
				isSubmit={false}
				from=""
				to=""
				date=""
				setIsSubmit={() => {}}
				setFrom={() => {}}
				setTo={() => {}}
				setDate={() => {}}
			/>
		);
		const fromText = screen.getByText(/From/i);
		expect(fromText).toBeInTheDocument();
	});

	it("Check if class is left-pane when isSubmit is true", () => {
		render(
			<MockSidebar
				isSubmit={true}
				from=""
				to=""
				date=""
				setIsSubmit={() => {}}
				setFrom={() => {}}
				setTo={() => {}}
				setDate={() => {}}
			/>
		);
		const sectionPane = screen.getByTestId("sidebar-section");
		expect(sectionPane).toHaveClass("left-pane");
	});
});
