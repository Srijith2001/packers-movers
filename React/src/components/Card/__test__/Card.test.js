import { render, screen } from "@testing-library/react";
import Card from "../Card";

const props = [
	{
		name: "Truck",
		desc: "Can carry upto 2000 kgs.",
		image: "",
		vehiclePrice: "₹ 2000",
		pricePKm: "₹ 40",
		onClick: jest.fn(),
	},
	{
		name: "Tractor",
		desc: "Can carry upto 5000 kgs.",
		image: "",
		vehiclePrice: "₹ 2500",
		pricePKm: "₹ 45",
		onClick: jest.fn(),
	},
	{
		name: "123",
		desc: "",
		image: "",
		vehiclePrice: "",
		pricePKm: "",
		onClick: jest.fn(),
	},
];

describe("Card", () => {
	it("renders card with the given props", () => {
		for (let i = 0; i < props.length; i++) {
			render(<Card {...props[i]} />);
			const truckName = screen.getByText(props[i].name);
			expect(truckName).toBeInTheDocument();
		}
	});
});
