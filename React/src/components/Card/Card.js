import React from "react"

export default function Card(props) {
    return (
        <div className="card" onClick={props.onClick}>
            <img src={props.image} className="card-image" alt={props.name}/>
            <p className="card-title">{props.name}</p>
            <p className="card-desc">{props.desc}</p>
            <p className="card-price"><span className="bold">Price: </span>{props.vehiclePrice} + {props.pricePKm}/km</p>
        </div>
    )
}