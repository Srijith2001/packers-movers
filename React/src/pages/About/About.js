import React from "react";

export default function About() {
    return (
        <div className="about">
            <h3>About us</h3>
            <br />
            <p>This is a simple packers and movers app which is developed using React as front-end, 
                Golang as back-end and MongoDB as database. <br/>
                Apart from that, we have used Kafka for storing the order queue before writing into the database
                and Redis for storing the order details.
            </p>
        </div>
    )
}