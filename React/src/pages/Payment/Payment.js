import React from "react";
import Order from "../Order/Order";
import BarLoader from "../../components/BarLoader/BarLoader";
import { Button, MenuItem, Select } from "@mui/material";
import { Route, Routes, useNavigate } from "react-router-dom";

export default function Payment() {
	const navigate = useNavigate();

	const [orderDetails, setOrderDetails] = React.useState({});
	const [isMounted, setIsMounted] = React.useState(false);
	const [paymentMethod, setPaymentMethod] = React.useState("cash");

	const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

	React.useEffect(() => {
		async function getDetails() {
			const res = await fetch("/estimated-cost");
			const data = await res.json();
			setOrderDetails(data.message);
			setIsMounted(true);
		}
		sleep(5000).then(() => getDetails());
	}, []);

	function submitButton() {
		const requestOptions = {
			method: "post",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({
				ptype: paymentMethod,
			}),
		};

		fetch("http://localhost:8080/order", requestOptions)
			.then((response) => response.json())
			.then((data) => {
				console.log(data);
			});

		navigate("order");
	}
	function handleChangePaymentMethod(event) {
		setPaymentMethod(event.target.value);
	}
	if (isMounted) {
		if (orderDetails.isAvailable) {
			return (
				<Routes>
					<Route
						path="*"
						element={
							<div className="order-details">
								<h3>Confirm Order</h3>
								<div className="user-info">
									<p>Name: {orderDetails.User.name}</p>
									<p>Email: {orderDetails.User.email}</p>
									<p>Contact: {orderDetails.User.phone}</p>
								</div>
								<h4>Trip locations</h4>
								<div className="user-locations">
									<div className="location">
										<h4>From:</h4>
										<p>
											{orderDetails.From.door},<br />
											{orderDetails.From.street},<br />
											{orderDetails.From.city},<br />
											{orderDetails.From.state},<br />
											{orderDetails.From.zip}
										</p>
									</div>
									<div className="location">
										<h4>To:</h4>
										<p>
											{orderDetails.To.door},<br />
											{orderDetails.To.street},<br />
											{orderDetails.To.city},<br />
											{orderDetails.To.state},<br />
											{orderDetails.To.zip}
										</p>
									</div>
								</div>
								<h4>Trip Details</h4>
								<div className="trip-details">
									<p>Vehicle Type: {orderDetails.vtype}</p>
									<p>
										Journey Date & Time:{" "}
										{orderDetails.DateTime.date} -{" "}
										{orderDetails.DateTime.time} am
									</p>
									<p>
										<b>Price:</b> Rs.{" "}
										{orderDetails.TotalCost} <br />
										{orderDetails.assist
											? "Requires Assistance"
											: null}
									</p>
									<h5>Payment Method</h5>
									<Select
										value={paymentMethod}
										label="Payment Method"
										onChange={(event) =>
											handleChangePaymentMethod(event)
										}
									>
										<MenuItem value="card">
											Debit/Credit card
										</MenuItem>
										<MenuItem value="upi">UPI</MenuItem>
										<MenuItem value="cash">
											Pay After Trip
										</MenuItem>
									</Select>
									<div className="order-submit">
										<Button
											variant="contained"
											onClick={submitButton}
										>
											Confirm Booking
										</Button>
									</div>
								</div>
							</div>
						}
					/>
					<Route path="order/*" element={<Order />} />
				</Routes>
			);
		} else {
			return (
				<h4 style={{ margin: "20px" }}>
					We are sorry! We don't have any vehicles availale for your
					location.
				</h4>
			);
		}
	} else {
		return <BarLoader customText="Loading..." />;
	}
}
