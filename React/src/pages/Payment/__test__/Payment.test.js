import { render, screen } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import { BrowserRouter } from "react-router-dom";
import Payment from "../Payment";

const MockPayment = () => {
	return (
		<BrowserRouter>
			<Payment />
		</BrowserRouter>
	);
};

describe("Payment", () => {
	it("should render loading component for 5s", () => {
		render(<MockPayment />);
		expect(screen.getByText("Loading...")).toBeInTheDocument();
	});

	it("Check if orderDetails are properly rendered from api call", async () => {
		// Create test case for payment page
		const fakeOrder = {
			// Vehicle availale test case
			message: {
				isAvailable: true,
				User: {
					name: "John Doe",
					email: "johndoe@gmail.com",
					contact: "1234567890",
				},
				From: {
					door: "A",
					street: "Street",
					city: "City",
					state: "State",
					pincode: "123456",
				},
				To: {
					door: "B",
					street: "Street",
					city: "City",
					state: "State",
					pincode: "123456",
				},
				DateTime: {
					date: "2020-06-01",
					time: "10:00",
				},
				vtype: "Trailer",
				TotalCost: 6000,
				assist: true,
			},
		};

		jest.spyOn(global, "fetch").mockImplementation(() =>
			Promise.resolve({
				json: () => Promise.resolve(fakeOrder),
			})
		);

		// eslint-disable-next-line testing-library/no-unnecessary-act
		await act(async () => {
			render(<MockPayment />);
		});

		jest.setTimeout(() => {
			expect(screen.getByText("John Doe")).toBeInTheDocument();
		}, 5000);
	});

	it("Check if no vehicles text is shown if there are no vehicles availale for that location", async () => {
		// Create empty test case
		const fakeOrder = {
			// Vehicle Unailable test case
			message: {
				isAvailable: false,
				User: {
					name: "",
					email: "",
					contact: "",
				},
				From: {
					door: "",
					street: "",
					city: "",
					state: "",
					pincode: "",
				},
				To: {
					door: "",
					street: "",
					city: "",
					state: "",
					pincode: "",
				},
				DateTime: {
					date: "",
					time: "",
				},
				vtype: "",
				TotalCost: 0,
				assist: false,
			},
		};

		jest.spyOn(global, "fetch").mockImplementation(() =>
			Promise.resolve({
				json: () => Promise.resolve(fakeOrder),
			})
		);

		// eslint-disable-next-line testing-library/no-unnecessary-act
		await act(async () => {
			render(<MockPayment />);
		});

		jest.setTimeout(() => {
			expect(screen.getByText(/we are sorry/i)).toBeInTheDocument();
		}, 5000);
	});
});
