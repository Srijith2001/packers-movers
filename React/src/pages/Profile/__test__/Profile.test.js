import { render, screen } from "@testing-library/react";
import Profile from "../Profile";

const props = [
	{
		Customer: {
			name: "John Doe",
			email: "john@gmail.com",
			phone: "1234567890",
		},
		Orders: null,
		TypeOfUser: "customer",
	},
	{
		Customer: {
			name: "test2",
			email: "test@gmail.com",
			phone: "1234567890",
		},
		Orders: [
			{
				id: "order-id-1",
				Bill: 2000,
				From: {
					door: "",
					street: "",
					city: "",
				},
				To: {
					door: "",
					street: "",
					city: "",
				},
				DateTime: {
					date: "",
				},
			},
		],
		TypeOfUser: "customer",
	},
];

describe("Profile", () => {
	// Check if profile is rendered for two different cases
	it("should render profile for customer with no orders", () => {
		render(<Profile {...props[0]} />);
		expect(screen.getByText(/John Doe/i)).toBeInTheDocument();
		expect(
			screen.getByText(/you have placed no orders/i)
		).toBeInTheDocument();
	});

	it("should render profile for customers with orders", () => {
		render(<Profile {...props[1]} />);
		expect(screen.getByText(/test2/i)).toBeInTheDocument();
		expect(screen.getByText(/order-id-1/i)).toBeInTheDocument();
	});
});
