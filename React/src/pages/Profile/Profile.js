import React from "react";
import OrderEle from "../../components/OrderEle/OrderEle";

export default function Profile(props) {
	const orders = props.Orders;

	const [orderEle, setOrderEle] = React.useState([]);

	React.useEffect(() => {
		if (orders != null) {
			var ordersEle = orders.map((order) => {
				return (
					<OrderEle
						order={order}
						TypeOfUser={props.TypeOfUser}
						key={order.id}
					/>
				);
			});
		}
		setOrderEle(ordersEle);
	}, []);
	return (
		<div className="user-profile">
			<h4>Profile</h4>
			<div className="user-info">
				<p>Name: {props.Customer.name}</p>
				<p>Email: {props.Customer.email}</p>
				<p>Contact: {props.Customer.phone}</p>
			</div>
			<h4>Order History</h4>
			{orders != null ? (
				<div className="order-info">{orderEle}</div>
			) : (
				<p>You have placed no orders</p>
			)}
		</div>
	);
}
