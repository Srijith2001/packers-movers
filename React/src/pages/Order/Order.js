import React from "react";
import BarLoader from "../../components/BarLoader/BarLoader";
import { Route, Routes } from "react-router-dom";

export default function Order() {
	const [details, setDetails] = React.useState({});
	const [isMounted, setIsMounted] = React.useState(false);

	React.useEffect(() => {
		fetch("http://localhost:8080/trip-details")
			.then((response) => response.json())
			.then((data) => {
				setDetails(data.message);
				setIsMounted(true);
			});
	}, []);

	if (isMounted) {
		return (
			<Routes>
				<Route
					path="*"
					element={
						<div className="booked-status">
							<h4>Trip Booked</h4>
							<br />
							<h5>Pickup Vehicle Details:</h5>
							<br />
							<p>Driver: {details.driverDetails.name}</p>
							<p>Contact: {details.driverDetails.contact}</p>
							<p>
								Vehicle: {details.nearestVehicle.brand}{" "}
								{details.nearestVehicle.model}
							</p>
							<p>
								Registration No:{" "}
								{details.nearestVehicle.vehicle_no}
							</p>
							<p>
								Pickup Time:{" "}
								{details.orderDetails.DateTime.date}{" "}
								{details.orderDetails.DateTime.time}am
							</p>
						</div>
					}
				/>
			</Routes>
		);
	} else {
		return <BarLoader customText="Loading..." />;
	}
}
