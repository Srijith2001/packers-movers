import { render, screen } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import { BrowserRouter } from "react-router-dom";
import Order from "../Order";

const MockOrder = () => {
	return (
		<BrowserRouter>
			<Order />
		</BrowserRouter>
	);
};
describe("Order", () => {
	it("Render the order details fetched from api", async () => {
		const fakeOrder = {
			message: {
				driverDetails: {
					name: "John Doe",
					contact: "1234567890",
				},
				nearestVehicle: {
					brand: "Toyota",
					model: "Corolla",
					vehicle_no: "KA-01-1234",
				},
				orderDetails: {
					DateTime: {
						date: "2020-06-01",
						time: "10:00",
					},
				},
			},
		};
		jest.spyOn(global, "fetch").mockImplementation(() =>
			Promise.resolve({
				json: () => Promise.resolve(fakeOrder),
			})
		);

		// eslint-disable-next-line testing-library/no-unnecessary-act
		await act(async () => {
			render(<MockOrder />);
		});

		expect(screen.getByText("Trip Booked")).toBeInTheDocument();
		expect(screen.getByText("Driver: John Doe")).toBeInTheDocument();
		expect(screen.getByText("Contact: 1234567890")).toBeInTheDocument();
		expect(screen.getByText("Vehicle: Toyota Corolla")).toBeInTheDocument();
		expect(
			screen.getByText("Registration No: KA-01-1234")
		).toBeInTheDocument();
		expect(
			screen.getByText("Pickup Time: 2020-06-01 10:00am")
		).toBeInTheDocument();

		// remove the mock to ensure tests are completely isolated
		global.fetch.mockRestore();
	});
});
