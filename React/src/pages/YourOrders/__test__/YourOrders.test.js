import { render, screen } from "@testing-library/react";
import YourOrders from "../YourOrders";

const props = [
	{
		Driver: {
			name: "John Doe",
			email: "john@gmail.com",
			contact: "1234567890",
		},
		Orders: null,
		TypeOfUser: "driver",
	},
	{
		Driver: {
			name: "test2",
			email: "test@gmail.com",
			contact: "1234567890",
		},
		Orders: [
			{
				id: "order-id-1",
				Bill: 2000,
				From: {
					door: "",
					street: "",
					city: "",
				},
				To: {
					door: "",
					street: "",
					city: "",
				},
				DateTime: {
					date: "",
				},
			},
		],
		TypeOfUser: "driver",
	},
];

describe("YourOrders", () => {
	// Check if profile is rendered for two different cases
	it("should render profile for customer with no orders", () => {
		render(<YourOrders {...props[0]} />);
		expect(screen.getByText(/John Doe/i)).toBeInTheDocument();
		expect(screen.getByText(/you have no orders/i)).toBeInTheDocument();
	});

	it("should render profile for customers with orders", () => {
		render(<YourOrders {...props[1]} />);
		expect(screen.getByText(/test2/i)).toBeInTheDocument();
		expect(screen.getByText(/order-id-1/i)).toBeInTheDocument();
	});
});
