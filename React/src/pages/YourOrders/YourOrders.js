import React from "react";
import OrderEle from "../../components/OrderEle/OrderEle";

export default function YourOrders(props) {
	const orders = props.Orders;

	const [orderEle, setOrderEle] = React.useState([]);

	React.useEffect(() => {
		if (orders != null) {
			var ordersEle = orders.map((order) => {
				return (
					<OrderEle
						order={order}
						TypeOfUser={props.TypeOfUser}
						key={order.id}
					/>
				);
			});
		}
		setOrderEle(ordersEle);
	}, []);

	return (
		<div style={{ margin: "20px" }}>
			<h4>About you:</h4>
			<div className="user-info">
				<p>Name: {props.Driver.name}</p>
				<p>Email: {props.Driver.email}</p>
				<p>Contact: {props.Driver.contact}</p>
			</div>
			<hr></hr>
			<h4 style={{ marginTop: "20px" }}>Your Trips</h4>
			{orders != null ? (
				<div className="order-info">{orderEle}</div>
			) : (
				<p>You have no orders</p>
			)}
		</div>
	);
}
