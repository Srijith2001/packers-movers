import React from "react";

export default function Contact() {
    return (
        <div className="contact">
            <h3>Contact us</h3>
            <br />
            <p> Mail: srijithharadwaj.d@lynk.co.in </p>
            <p> Whatsapp: 8754113045</p>
        </div>
    )
}