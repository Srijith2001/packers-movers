import { fireEvent, render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import HomePage from "../HomePage";
const props = {
	signInDialog: false,
	signUpDialog: false,
	isSignedIn: false,
	handleSignInClick: jest.fn(),
	handleSignUpClick: jest.fn(),
	handleSignInClose: jest.fn(),
	handleSignUpClose: jest.fn(),
	handleSignInChange: jest.fn(),
	handleUserChangeInfo: jest.fn(),
	signInDetails: {
		email: "",
		password: "",
	},
	userDetails: {},
	vehicleDetails: {},
	address: {},
	typeOfUser: "",
	setTypeOfUser: jest.fn(),
	handleChangeVehicleInfo: jest.fn(),
	handleAddressChange: jest.fn(),
	signIn: jest.fn(),
	signUp: jest.fn(),
};

const MockHomePage = (props) => {
	return (
		<BrowserRouter>
			<HomePage {...props} />
		</BrowserRouter>
	);
};

describe("HomePage", () => {
	it("Check if everything renders properly", () => {
		render(<MockHomePage {...props} />);
		expect(screen.getByText(/Looking for a truck/i)).toBeInTheDocument();
	});

	it("Check if handleSignInClick function is called when sign in button is pressed", () => {
		render(<MockHomePage {...props} />);
		const signInButton = screen.getByRole("button", { name: "Sign In" });
		fireEvent.click(signInButton);
		expect(props.handleSignInClick).toHaveBeenCalled();
	});

	it("Check if handleSignUpClick function is called when sign up button is pressed", () => {
		render(<MockHomePage {...props} />);
		const signUpButton = screen.getByRole("button", { name: "Sign Up" });
		fireEvent.click(signUpButton);
		expect(props.handleSignUpClick).toHaveBeenCalled();
	});
});
