import React from "react"
import { Dialog, DialogContent, DialogTitle, MenuItem, Select } from "@mui/material";

export default function HomePage({
    signInDialog, 
    signUpDialog, 
    isSignedIn, 
    handleSignInClick,
    handleSignUpClick,
    handleSignInClose,
    handleSignUpClose,
    handleSignInChange,
    handleUserChangeInfo,
    signInDetails,
    userDetails,
    vehicleDetails,
    address,
    typeOfUser,
    setTypeOfUser,
    handleChangeVehicleInfo,
    handleAddressChange,
    signIn,
    signUp}){

    return (
        <div className="homepage">
            <h3>Looking for a truck for shifting you home?</h3>
            <h4>You are in the right place!</h4>
            <p>Please create an account if you are new or sign in if you are already a customer.</p>
            <>
                <button className="nav-btn" onClick={handleSignInClick}>Sign In</button>
                <Dialog open={signInDialog} onClose={handleSignInClose}>
                    <DialogTitle>Sign In</DialogTitle>
                    <DialogContent>
                        <form>
                            <div className="form-group">
                                <label htmlFor="email">Email address</label>
                                <input 
                                    type="email" 
                                    className="form-control" 
                                    id="email" 
                                    aria-describedby="emailHelp" 
                                    placeholder="Enter email"
                                    name="email"
                                    value={signInDetails.email} 
                                    onChange={handleSignInChange}
                                />
                                <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <input 
                                    type="password" 
                                    className="form-control" 
                                    id="password" 
                                    placeholder="Password" 
                                    name="password"
                                    value={signInDetails.password}
                                    onChange={handleSignInChange}
                                />
                            </div>
                            <button type="submit" className="btn btn-primary" onClick={signIn}>Submit</button>
                        </form>
                    </DialogContent>
                </Dialog>
                <button className="nav-btn nav-sign-up" onClick={handleSignUpClick}>Sign Up</button>
                <Dialog open={signUpDialog} onClose={handleSignUpClose}>
                    <DialogTitle>Sign Up</DialogTitle>
                    <DialogContent>
                        <form>
                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <input 
                                    type="text" 
                                    className="form-control" 
                                    id="name" 
                                    placeholder="Enter name" 
                                    name="name"
                                    value={userDetails.name}
                                    onChange={handleUserChangeInfo}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="email">Email address</label>
                                <input 
                                    type="email" 
                                    className="form-control" 
                                    id="email" 
                                    aria-describedby="emailHelp" 
                                    placeholder="Enter email"
                                    name="email"
                                    value={userDetails.email}
                                    onChange={handleUserChangeInfo}
                                />
                                <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <input 
                                    type="password" 
                                    className="form-control" 
                                    id="password" 
                                    placeholder="Password"
                                    name="password"
                                    value={userDetails.password}
                                    onChange={handleUserChangeInfo}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="phone">Phone</label>
                                <input 
                                    type="text" 
                                    className="form-control" 
                                    id="phone" 
                                    placeholder="Enter phone"
                                    name="contact"
                                    value={userDetails.contact}
                                    onChange={handleUserChangeInfo}
                                />
                            </div>
                            <div className="form-group">
                                <label>Type of User:</label><br/>
                                <Select
                                    style={{"height": "40px", fontSize: "0.8rem"}}
                                    value={typeOfUser}
                                    labelId = "user-type"
                                    label="Type"
                                    onChange={(e) => setTypeOfUser(e.target.value)}
                                >
                                    <MenuItem value="customer">Customer</MenuItem>
                                    <MenuItem value="driver">Driver</MenuItem>
                                </Select>
                            </div>
                            {typeOfUser === "driver" ? 
                                <div className="driver">
                                    <div className="vehicle-details">
                                        <h4>Vehicle Details</h4>
                                        <div className="form-group">
                                            <label htmlFor="vehicle_no">Vehicle No.</label>
                                            <input
                                                type="text"
                                                id="vehicle_no"
                                                className="form-control"
                                                placeholder="Enter your vehicle no."
                                                value={vehicleDetails.vehicle_no}
                                                name="vehicle_no"
                                                onChange={handleChangeVehicleInfo}/>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="brand">Brand</label>
                                            <input
                                                type="text"
                                                id="brand"
                                                className="form-control"
                                                placeholder="Enter your vehicle brand"
                                                value={vehicleDetails.brand}
                                                name="brand"
                                                onChange={handleChangeVehicleInfo}/>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="model">Model</label>
                                            <input
                                                type="text"
                                                id="model"
                                                className="form-control"
                                                placeholder="Enter your vehicle model"
                                                value={vehicleDetails.model}
                                                name="model"
                                                onChange={handleChangeVehicleInfo}/>
                                        </div>
                                        <div className="form-group">
                                            <label id="vehicle-type">Type</label>
                                            <Select
                                                style={{"height": "40px", fontSize: "0.8rem"}}
                                                className="form-control"
                                                value={vehicleDetails.type}
                                                labelId = "vehicle-type"
                                                label="Vehicle Type"
                                                name="type"
                                                onChange={handleChangeVehicleInfo}
                                            >
                                                <MenuItem value="3 Wheeler">3 Wheeler</MenuItem>
                                                <MenuItem value="Pickup">Pickup</MenuItem>
                                                <MenuItem value="Mini Truck">Mini Truck</MenuItem>
                                                <MenuItem value="Truck">Truck</MenuItem>
                                                <MenuItem value="Tractor">Tractor</MenuItem>
                                                <MenuItem value="Trailer">Trailer</MenuItem>
                                            </Select>
                                        </div>
                                    </div>
                                    <div className="address-details">
                                        <h4>Your Address</h4>
                                        <div className="form-group">
                                            <label htmlFor="door">Door No.</label>
                                            <input
                                                type="text"
                                                id="door"
                                                className="form-control"
                                                placeholder="Enter your door no."
                                                value={address.door}
                                                name="door"
                                                onChange={handleAddressChange}/>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="street">Street</label>
                                            <input
                                                type="text"
                                                id="street"
                                                className="form-control"
                                                placeholder="Enter your street"
                                                value={address.street}
                                                name="street"
                                                onChange={handleAddressChange}/>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="city">City</label>
                                            <input
                                                type="text"
                                                id="city"
                                                className="form-control"
                                                placeholder="Enter your city"
                                                value={address.city}
                                                name="city"
                                                onChange={handleAddressChange}/>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="state">State</label>
                                            <input
                                                type="text"
                                                id="state"
                                                className="form-control"
                                                placeholder="Enter your state"
                                                value={address.state}
                                                name="state"
                                                onChange={handleAddressChange}/>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="pincode">Pincode</label>
                                            <input
                                                type="text"
                                                id="zip"
                                                className="form-control"
                                                placeholder="Enter your pincode"
                                                value={address.zip}
                                                name="zip"
                                                onChange={handleAddressChange}/>
                                        </div>
                                    </div>
                                </div>
                            : null}
                            <button type="submit" className="btn btn-primary" onClick={signUp}>Submit</button>
                        </form>
                    </DialogContent>
                </Dialog>
            </>
        </div>
    )
}