import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import UserDetails from "../UserDetails";

const MockUserDetails = (props) => {
	return (
		<BrowserRouter>
			<UserDetails {...props} />
		</BrowserRouter>
	);
};

describe("UserDetails", () => {
	it("should render userdetails form page with props", () => {
		const props = {
			userDetails: {
				name: "testName",
				email: "test@gmail.com",
				phone: "1234567890",
				id: "test-id",
			},
			from: "chennai",
			to: "bangalore",
			date: "2020-01-01",
		};
		render(<MockUserDetails {...props} />);
		expect(screen.getByText(/testName/i)).toBeInTheDocument();
		expect(screen.getByText(/test@gmail.com/i)).toBeInTheDocument();
		expect(screen.getByText(/1234567890/i)).toBeInTheDocument();
	});
});
