import React from "react";
import Payment from "../Payment/Payment";

import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import { DateTimePicker } from "@mui/lab";
import { Button, Checkbox, FormControlLabel, TextField } from "@mui/material";
import moment from "moment";
import { Route, Routes, useNavigate } from "react-router-dom";

export default function UserDetails(props) {
	const navigate = useNavigate();

	const [userInfo, setUserInfo] = React.useState({
		name: props.userDetails.name,
		email: props.userDetails.email,
		phone: props.userDetails.phone,
		id: props.userDetails.id,
	});

	const [fromLocation, setFromLocation] = React.useState({
		door: "",
		street: "",
		city: props.from,
		state: "",
		zip: "",
	});

	const [toLocation, setToLocation] = React.useState({
		door: "",
		street: "",
		city: props.to,
		state: "",
		zip: "",
	});

	const [pickupTime, setPickupTime] = React.useState({
		date: props.date,
		time: "",
	});

	const [isChecked, setIsChecked] = React.useState(false);

	function handleChangeFromLocation(event) {
		setFromLocation((prevFormData) => {
			return {
				...prevFormData,
				[event.target.name]: event.target.value,
			};
		});
	}

	function handleChangeToLocation(event) {
		setToLocation((prevFormData) => {
			return {
				...prevFormData,
				[event.target.name]: event.target.value,
			};
		});
	}
	function handleChangeDate(event) {
		setPickupTime({
			date: moment(event).format("DD/MM/YYYY"),
			time: moment(event).format("HH:mm"),
		});
		props.setDate(event);
	}

	function handleChange() {
		setIsChecked(!isChecked);
	}

	function handleSubmit() {
		const requestOptions = {
			method: "post",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({
				from: fromLocation,
				to: toLocation,
				datetime: pickupTime,
				user: userInfo,
				vtype: props.card,
				assist: isChecked,
			}),
		};

		fetch("http://localhost:8080/payment", requestOptions)
			.then((response) => response.json())
			.then((data) => {
				console.log(data);
			});

		navigate("payment");
	}
	return (
		<Routes>
			<Route
				path="*"
				element={
					<div style={{ margin: "20px" }}>
						<h3>Checkout</h3>
						<div className="user-details">
							<form>
								<div className="user-info">
									<h4>Customer Information</h4>
									<br />
									<p>
										<b>Name:</b> {userInfo.name}
									</p>
									<p>
										<b>Email:</b> {userInfo.email}
									</p>
									<p>
										<b>Phone:</b> {userInfo.phone}
									</p>
								</div>

								<div className="user-locations">
									<div className="location">
										<h4>From</h4>
										<label
											htmlFor="fromDoorNo"
											className="checkout-label"
										>
											Door No
										</label>
										<input
											type="text"
											id="fromDoorNo"
											className="checkout-input"
											placeholder="Enter door no"
											value={fromLocation.door}
											name="door"
											onChange={handleChangeFromLocation}
										/>

										<label
											htmlFor="fromStreet"
											className="checkout-label required"
										>
											Street
										</label>
										<input
											type="text"
											id="fromStreet"
											className="checkout-input"
											placeholder="Enter street"
											value={fromLocation.street}
											name="street"
											required
											onChange={handleChangeFromLocation}
										/>

										<label
											htmlFor="fromCity"
											className="checkout-label required"
										>
											City
										</label>
										<input
											type="text"
											id="fromCity"
											className="checkout-input"
											value={fromLocation.city}
											name="city"
											required
											onChange={handleChangeFromLocation}
										/>

										<label
											htmlFor="fromState"
											className="checkout-label required"
										>
											State
										</label>
										<input
											type="text"
											id="fromState"
											className="checkout-input"
											placeholder="Enter state"
											value={fromLocation.state}
											name="state"
											required
											onChange={handleChangeFromLocation}
										/>

										<label
											htmlFor="fromZip"
											className="checkout-label required"
										>
											Pincode
										</label>
										<input
											type="text"
											id="fromZip"
											className="checkout-input"
											placeholder="Enter pincode"
											value={fromLocation.zip}
											name="zip"
											required
											onChange={handleChangeFromLocation}
										/>
									</div>

									<div className="location">
										<h4>To</h4>
										<label
											htmlFor="toDoorNo"
											className="checkout-label"
										>
											Door No
										</label>
										<input
											type="text"
											id="toDoorNo"
											className="checkout-input"
											placeholder="Enter door no"
											value={toLocation.door}
											name="door"
											onChange={handleChangeToLocation}
										/>

										<label
											htmlFor="toStreet"
											className="checkout-label required"
										>
											Street
										</label>
										<input
											type="text"
											id="toStreet"
											className="checkout-input"
											placeholder="Enter street"
											value={toLocation.street}
											name="street"
											required
											onChange={handleChangeToLocation}
										/>

										<label
											htmlFor="toCity"
											className="checkout-label required"
										>
											City
										</label>
										<input
											type="text"
											id="toCity"
											className="checkout-input"
											value={toLocation.city}
											name="city"
											required
											onChange={handleChangeToLocation}
										/>

										<label
											htmlFor="toState"
											className="checkout-label required"
										>
											State
										</label>
										<input
											type="text"
											id="toState"
											className="checkout-input "
											placeholder="Enter state"
											value={toLocation.state}
											name="state"
											required
											onChange={handleChangeToLocation}
										/>

										<label
											htmlFor="toZip"
											className="checkout-label required"
										>
											Pincode
										</label>
										<input
											type="text"
											id="toZip"
											className="checkout-input "
											placeholder="Enter pincode"
											value={toLocation.zip}
											name="zip"
											required
											onChange={handleChangeToLocation}
										/>
									</div>
								</div>
								<div className="checkout-journey">
									<h5>Journey details</h5>
									<div className="checkout-date">
										<LocalizationProvider
											dateAdapter={AdapterDateFns}
										>
											<DateTimePicker
												label="Date and Time of pickup"
												type="datetime-local"
												value={props.date}
												onChange={(event) =>
													handleChangeDate(event)
												}
												renderInput={(params) => (
													<TextField {...params} />
												)}
												inputFormat="dd/MM/yyyy h:mm a"
											/>
										</LocalizationProvider>
									</div>

									<FormControlLabel
										checked={isChecked}
										onChange={handleChange}
										control={<Checkbox color="primary" />}
										label="Need Assistance in packing(Rs.1000 extra)"
									/>
									<div className="checkout-submit">
										<Button
											variant="contained"
											onClick={handleSubmit}
										>
											Proceed to Payment
										</Button>
									</div>
								</div>
							</form>
						</div>
					</div>
				}
			/>
			<Route path="payment/*" element={<Payment />} />
		</Routes>
	);
}
