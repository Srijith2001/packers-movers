import React from "react";
import { Route, Routes } from "react-router-dom";
import Split from "react-split";

import Sidebar from "../../components/Sidebar/Sidebar";
import Trucks from "../../components/Trucks/Trucks";
import UserDetails from "../UserDetails/UserDetails";

export default function HomeShifting(props) {
	const [from, setFrom] = React.useState(""); // From input
	const [to, setTo] = React.useState(""); // To input
	const [date, setDate] = React.useState(new Date()); // Date input
	const [isSubmit, setIsSubmit] = React.useState(false); // Submit button
	const [card, setCard] = React.useState(""); // Vehicle type

	const postObject = {
		from: from,
		to: to,
		date: date,
		setDate: setDate,
		card: card,
		userDetails: props.Customer,
	};

	const properties = {
		from: from,
		to: to,
		date: date,
		setDate: setDate,
		setFrom: setFrom,
		setTo: setTo,
		isSubmit: isSubmit,
		setIsSubmit: setIsSubmit,
		card: card,
		setCard: setCard,
	};
	return (
		<main>
			<Routes>
				<Route
					path="trucks"
					element={
						<Split
							sizes={[25, 75]}
							direction="horizontal"
							gutterSize={10}
							gutterAlign="center"
							className="split"
						>
							<Sidebar {...properties} />
							<Trucks setCard={properties.setCard} />
						</Split>
					}
				/>
				<Route path="/" element={<Sidebar {...properties} />} />
				<Route
					path="trucks/user-details/*"
					element={<UserDetails {...postObject} />}
				/>
			</Routes>
		</main>
	);
}
