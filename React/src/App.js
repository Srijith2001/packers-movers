import React from "react";

import "./App.css";
import Navbar from "./components/Navbar/Navbar";
import { Routes, Route, useNavigate } from "react-router-dom";
import { nanoid } from "nanoid";

import HomeShifting from "./pages/HomeShifting/HomeShifting";
import About from "./pages/About/About";
import Contact from "./pages/Contact/Contact";
import Profile from "./pages/Profile/Profile";
import HomePage from "./pages/HomePage/HomePage";
import YourOrders from "./pages/YourOrders/YourOrders";

export default function App() {
	const [menuTab, setMenuTab] = React.useState("Home"); // Navbar menu tab

	const menuList = [
		{
			val: "Home",
			href: "/",
			active: menuTab === "Home",
		},
		{
			val: "HomeShifting",
			href: "/homeshifting",
			active: menuTab === "HomeShifting",
		},
		{
			val: "About",
			href: "/about",
			active: menuTab === "About",
		},
		{
			val: "Contact",
			href: "/contact",
			active: menuTab === "Contact",
		},
	];

	const [properties, setProperties] = React.useState({});
	const navigate = useNavigate();
	const [signInDialog, setSignInDialog] = React.useState(false);
	const [signUpDialog, setSignUpDialog] = React.useState(false);
	const [isSignedIn, setIsSignedIn] = React.useState(false);
	const [typeOfUser, setTypeOfUser] = React.useState("customer");

	const [signInDetails, setSignInDetails] = React.useState({
		email: "",
		password: "",
	});

	const [userDetails, setUserDetails] = React.useState({
		name: "",
		email: "",
		contact: "",
		password: "",
		id: nanoid(),
	});

	const [vehicleDetails, setVehicleDetails] = React.useState({
		vehicle_no: "",
		brand: "",
		model: "",
		type: "",
	});

	const [address, setAddress] = React.useState({
		door: "",
		street: "",
		city: "",
		state: "",
		zip: "",
	});

	function handleUserChangeInfo(e) {
		setUserDetails({
			...userDetails,
			[e.target.name]: e.target.value,
		});
	}

	function handleChangeVehicleInfo(e) {
		setVehicleDetails({
			...vehicleDetails,
			[e.target.name]: e.target.value,
		});
	}

	function handleAddressChange(e) {
		setAddress({
			...address,
			[e.target.name]: e.target.value,
		});
	}

	const handleSignInClick = () => {
		setSignInDialog(true);
	};

	const handleSignInClose = () => {
		setSignInDialog(false);
	};

	const handleSignUpClick = () => {
		setSignUpDialog(true);
	};

	const handleSignUpClose = () => {
		setSignUpDialog(false);
	};

	const handleSignInChange = (e) => {
		setSignInDetails({
			...signInDetails,
			[e.target.name]: e.target.value,
		});
	};

	function signIn(e) {
		e.preventDefault();
		const requestOptions = {
			method: "post",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({
				email: signInDetails.email,
				password: signInDetails.password,
			}),
		};

		fetch("http://localhost:8080/sign-in", requestOptions)
			.then((response) => response.json())
			.then((data) => {
				if (data.message === "failed") {
					alert("Invalid email or password");
				} else {
					setProperties(data);
					if (data.TypeOfUser === "customer") {
						setMenuTab("Homeshifting");
						navigate("/homeshifting");
					} else if (data.TypeOfUser === "driver") {
						setMenuTab("Dashboard");
						navigate("/yourorders");
					}
				}
			});
		setIsSignedIn(true);
	}

	function signUp(e) {
		e.preventDefault();
		// Check if user is a driver/customer
		if (typeOfUser === "driver") {
			const requestOptions = {
				method: "post",
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify({
					driver: userDetails,
					vehicle: vehicleDetails,
					location: address,
				}),
			};
			console.log("submitted");
			fetch("http://localhost:8080/new-driver", requestOptions)
				.then((response) => response.json())
				.then((data) => {
					if (data.message === "Driver already exists") {
						alert("Driver already exists");
					} else {
						alert("Driver added successfully");
						setProperties(data);
						setMenuTab("Dashboard");
						setIsSignedIn(true);
						navigate("/yourorders");
					}
				});
		} else {
			const requestOptions = {
				method: "post",
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify({
					name: userDetails.name,
					email: userDetails.email,
					password: userDetails.password,
					phone: userDetails.contact,
					id: userDetails.id,
				}),
			};
			console.log(requestOptions);
			fetch("http://localhost:8080/new-customer", requestOptions)
				.then((response) => response.json())
				.then((data) => {
					if (data.message === "User already exists") {
						alert("User already exists");
					} else {
						alert("User Created");
						setProperties({
							...properties,
							Customer: data,
							TypeOfUser: "customer",
						});
						setMenuTab("Homeshifting");
						setIsSignedIn(true);
						navigate("/homeshifting");
					}
				});
		}
	}

	const loginProps = {
		signInDialog: signInDialog,
		signUpDialog: signUpDialog,
		isSignedIn: isSignedIn,
		handleSignInClick: handleSignInClick,
		handleSignUpClick: handleSignUpClick,
		handleSignInClose: handleSignInClose,
		handleSignUpClose: handleSignUpClose,
		handleSignInChange: handleSignInChange,
		handleUserChangeInfo: handleUserChangeInfo,
		signInDetails: signInDetails,
		userDetails: userDetails,
		vehicleDetails: vehicleDetails,
		address: address,
		typeOfUser: typeOfUser,
		setTypeOfUser: setTypeOfUser,
		handleChangeVehicleInfo: handleChangeVehicleInfo,
		handleAddressChange: handleAddressChange,
		signIn: signIn,
		signUp: signUp,
	};
	return (
		<>
			<Navbar
				menuList={menuList}
				menuTab={menuTab}
				setMenuTab={setMenuTab}
				setProperties={setProperties}
				isSignedIn={isSignedIn}
				typeOfUser={properties.TypeOfUser}
			/>

			<Routes>
				<Route exact path="/" element={<HomePage {...loginProps} />} />
				<Route
					exact
					path="/homeshifting/*"
					element={<HomeShifting {...properties} />}
				/>
				<Route exact path="/about" element={<About />} />
				<Route exact path="/contact" element={<Contact />} />
				<Route
					exact
					path="/profile"
					element={<Profile {...properties} />}
				/>
				<Route
					exact
					path="/yourorders"
					element={<YourOrders {...properties} />}
				/>
			</Routes>
		</>
	);
}
